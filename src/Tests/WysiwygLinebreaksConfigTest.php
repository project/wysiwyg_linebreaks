<?php

namespace Drupal\wysiwyg_linebreaks\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the functionality of the Wysiwyg Linebreaks module for an admin user.
 *
 * @group wysiwyg_linebreaks
 */
class WysiwygLinebreaksConfigTest extends BrowserTestBase {

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A node to use for testing.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Use the Standard profile so default text formats are enabled.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['wysiwyg_linebreaks'];

  /**
   * Setup function.
   */
  public function setUp(): void {
    // Enable modules required for this test.
    parent::setUp();

    // Set up admin user.
    $this->adminUser = $this->drupalCreateUser(['administer filters']);
  }

  /**
   * Test the text formats form.
   */
  public function testBasicHtmlTextFormatsConfigForm() {
    // Log in the admin user.
    $this->drupalLogin($this->adminUser);

    // Get the comment reply form and ensure there's no 'url' field.
    $this->drupalGet('admin/config/content/formats/manage/basic_html');
    $this->assertSession()->pageTextContains('Force linebreaks conversion method shown.');
    $this->assertSession()->pageTextContains('Convert linebreaks conversion method shown.');
  }

}
